import type { BoardgameRead } from "../../types"

export async function load({fetch}) {
    const response = await fetch('http://saboga-api:8000/games')
    const games = await response.json() as BoardgameRead[] ;

    return { games };
}